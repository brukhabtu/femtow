FROM python:3.8-alpine

WORKDIR content
RUN addgroup -S appuser \
    && adduser -S -G appuser appuser \
    && chown appuser /content \
    && apk add --no-cache build-base libffi-dev libressl-dev musl-dev libffi-dev dumb-init && pip install poetry

USER appuser

COPY femtow /content/femtow
COPY tests /content/tests
COPY poetry.lock pyproject.toml /content/

RUN poetry install

ENTRYPOINT ["dumb-init", "--", "poetry"]
EXPOSE 5000
