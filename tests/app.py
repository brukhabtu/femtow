import sys
from femtow import Femtow, route, view, json_renderer


@route(route_name="test get", route_path="/", methods=("GET",))
@view(renderer=json_renderer)
def test():
    return {"success": True}


application = Femtow()
application.scan(sys.modules[__name__])
