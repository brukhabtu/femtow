import re
from invoke import task

_DOCKER_IMAGE_TAG = "femtow"
_SEM_VER_REGEX = r"(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$"


@task
def build(c):
    c.run(f"docker build -t={_DOCKER_IMAGE_TAG} .")


@task
def shell(c):
    c.run(f"docker run -ti --publish 5000 --entrypoint=bash {_DOCKER_IMAGE_TAG}")


@task
def tag(c, version):
    poetry_version_results = c.run(f"poetry version {version}")
    project_version = re.search(_SEM_VER_REGEX, poetry_version_results.stdout.strip())[
        0
    ]
    c.run(f"git tag {project_version}")
    c.run("git add pyproject.toml")
    c.run(f"git commit -m 'Incrementing version to: {project_version}'")
    c.run("git push")
    c.run("git push --tags")
