## Getting Started

**Requirements**

* Python 3.8
* Docker (optional) 

```
$ python3.8 -m venv ~/.virtualenvs/femtow
$ source ~/.virtualenvs/femtow/bin/activate
$ pip install -e . ".[dev]"
$ pre-commit install
```

## Running Tests

**Docker**

```
$ make test-unit
$ make test-integration
``` 

**Local**

```
$ pytest tests/unit
$ pytest tests/integration
```
